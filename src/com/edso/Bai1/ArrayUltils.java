package com.edso.Bai1;

import java.util.ArrayList;
import java.util.List;

public class ArrayUltils {
    //Double Check Locking Singleton dung cho cac ung dung multi-thread

    private static volatile ArrayUltils instance;

    private ArrayUltils() {
    }

    public static ArrayUltils getInstance() {
        // Do something before get instance ...
        if (instance == null) {
            // Do the task too long before create instance ...
            // Block so other threads cannot come into while initialize
            synchronized (ArrayUltils.class) {
                // Re-check again. Maybe another thread has initialized before
                if (instance == null) {
                    instance = new ArrayUltils();
                }
            }
        }
        // Do something after get instance ...
        return instance;
    }

    public <T> void display(List<T> list){
        try {
            if(list.size() < 1){
                System.out.println("Your list is null");
                return;
            }
            for(T item : list){
                if(item == null || item.equals("")){
                    System.out.println("Your element is null");
                    return;
                }
                System.out.println(item);
            }
        }catch (NullPointerException exception){
            System.out.println("You have an error :"+exception);
        }

    }

    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);

        ArrayUltils.getInstance().display(list);
    }
}
