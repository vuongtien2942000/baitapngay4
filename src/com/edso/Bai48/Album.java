package com.edso.Bai48;

import java.util.ArrayList;
import java.util.LinkedList;

public class Album {
    private String name ;
    private String artist ;
    private SongList songs;

    public Album(String name , String artist) {
        this.name = name ;
        this.artist = artist ;
        this.songs = new SongList();
    }

    public boolean addSong(String title, double duration) {
            if(title != null){
                this.songs.add(new Song(title, duration));
                return true;
            }
            return false;
    }


    public boolean addToPlayList(int trackNumber, LinkedList<Song> playList) {
        Song checkedSong = this.songs.findSong(trackNumber);
        if (checkedSong != null) {
            playList.add(checkedSong);
            return true;
        }
        System.out.println("This album does not have a track " + trackNumber);
        return false;
    }

    public boolean addToPlayList(String title, LinkedList<Song> playList) {
        Song checkedSong = this.songs.findSong(title);
        if (checkedSong != null) {
            playList.add(checkedSong);
            return true;
        }
        System.out.println("The song " + title + " is not in this album");
        return false;
    }

    //class SongList
    public class SongList{
        private  ArrayList<Song> songs;

        private SongList() {
            this.songs = new ArrayList<>();
        }

        private boolean add(Song newSong){
            if(findSong(newSong.getTitle()) == null){
                System.out.println("Add song success");
                this.songs.add(newSong);
                return true;
            }
            System.out.println("The song already exists!");
            return false;
        }

        private Song findSong(String title){
            Song song = null;
            for(Song existingSong : this.songs){
               if(existingSong.getTitle().equals(title)){
                   return existingSong;
               }
            }
            return null;
        }

        private Song findSong(int trackNumber){
            if(trackNumber > 0){
                int index = trackNumber -1;
                Song song = null;
                if(index>=0 && index<this.songs.size()){
                    song = this.songs.get(index);
                    return  song;
                }
                return null;
            }
            return null;
        }
    }


}



